﻿using PAuction.BL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace PAuction.BL.Interfaces
{
    public interface ICarService : ICrud<CarModel>
    {
        public IEnumerable<CarModel> SearchByFilter(CarFilterSearchModel filter);
    }
}
