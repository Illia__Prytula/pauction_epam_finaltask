﻿using PAuction.BL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace PAuction.BL.Interfaces
{
    public interface ILotService : ICrud<LotModel>
    {
        public IEnumerable<LotModel> GetLotsByStartTime(DateTime startTime);
        public IEnumerable<LotModel> GetLotsByCountry(CountryModel country);
        public IEnumerable<LotModel> GetLotsByCarModel(string model);
    }
}
