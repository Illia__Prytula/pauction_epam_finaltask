﻿using PAuction.BL.Models;
using System;
using System.Collections.Generic;

namespace PAuction.BL.Interfaces
{
    public interface IUserService : ICrud<UserModel>
    {
        public IEnumerable<UserModel> GetUsersByRegistrationDate(DateTime registrationDate);
        public IEnumerable<UserModel> GetUsersByReputation(int minReputation);
    }
}
