﻿using AutoMapper;
using PAuction.BL.Interfaces;
using PAuction.BL.Models;
using PAuction.BL.Validation;
using PAuction.DAL.Entities;
using PAuction.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace PAuction.BL.Services
{
    public class UserService : IUserService
    {
        readonly IUnitOfWork _unitOfWork;
        readonly IMapper _mapper;
        public UserService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task AddAsync(UserModel model)
        {
            if (String.IsNullOrEmpty(model.Name) || String.IsNullOrEmpty(model.Phone) || String.IsNullOrEmpty(model.Surname)
                    || model.RegistrationDate == default || String.IsNullOrEmpty(model.Email))
                throw new PAuctionException("User model does not containt correct data.");
            var user = _mapper.Map<UserModel, User>(model);
            await _unitOfWork.UserRepository.AddAsync(user);
            await _unitOfWork.SaveAsync();
        }

        public async Task DeleteByIdAsync(int modelId)
        {
            await _unitOfWork.UserRepository.DeleteByIdAsync(modelId);
            await _unitOfWork.SaveAsync();
        }

        public IEnumerable<UserModel> GetAll()
        {
            var users = _unitOfWork.UserRepository.GetAllWithDetails().AsEnumerable();
            return _mapper.Map<IEnumerable<User>, IEnumerable<UserModel>>(users);
        }

        public async Task<UserModel> GetByIdAsync(int id)
        {
            return _mapper.Map<User, UserModel>(await _unitOfWork.UserRepository.GetByIdWithDetailsAsync(id));
        }

        public IEnumerable<UserModel> GetUsersByRegistrationDate(DateTime registrationDate)
        {
            var users = _unitOfWork.UserRepository.GetAllWithDetails().Where(x => x.RegistrationDate > registrationDate).AsEnumerable();
            return _mapper.Map<IEnumerable<User>, IEnumerable<UserModel>>(users);
        }

        public IEnumerable<UserModel> GetUsersByReputation(int minReputation)
        {
            var users = _unitOfWork.UserRepository.GetAllWithDetails().Where(x => x.Reputation > minReputation).AsEnumerable();
            return _mapper.Map<IEnumerable<User>, IEnumerable<UserModel>>(users);
        }

        public async Task UpdateAsync(UserModel model)
        {
            if (String.IsNullOrEmpty(model.Name) || String.IsNullOrEmpty(model.Phone) || String.IsNullOrEmpty(model.Surname)
                    || model.RegistrationDate == default || String.IsNullOrEmpty(model.Email))
                throw new PAuctionException("User model does not containt correct data.");

            var user = _mapper.Map<UserModel, User>(model);
            _unitOfWork.UserRepository.Update(user);
            await _unitOfWork.SaveAsync();
        }

        // use in registration in future
        public void Register(string password)
        {
            byte[] salt = new byte[128 / 8];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(salt);
            }
            string Salt = Convert.ToBase64String(salt);

            string Hashed = Convert.ToBase64String(Encoding.ASCII.GetBytes((Convert.ToBase64String(Encoding.ASCII.GetBytes(password)) + Salt)));

            // add salt and hash in db
        }

        public bool ComparePasswords(string hash, string salt, string password)
        {
            return (hash == Convert.ToBase64String(Encoding.ASCII.GetBytes((Convert.ToBase64String(Encoding.ASCII.GetBytes(password)) + salt))));
        }
    }
}
