﻿using AutoMapper;
using PAuction.BL.Interfaces;
using PAuction.BL.Models;
using PAuction.BL.Validation;
using PAuction.DAL.Entities;
using PAuction.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PAuction.BL.Services
{
    public class LotService : ILotService
    {
        readonly IUnitOfWork _unitOfWork;
        readonly IMapper _mapper;
        public LotService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task AddAsync(LotModel model)
        {
            if (model.StartTime == default || model.CurrentPrice <= 0 || model.BetTimeInHours <= 0 || model.UserIds.Count != 1)
                throw new PAuctionException("Lot model does not containt correct data.");
            var lot = _mapper.Map<LotModel, Lot>(model);
            await _unitOfWork.LotRepository.AddAsync(lot);
            await _unitOfWork.SaveAsync();
            await _unitOfWork.LotRegistrationRepository.AddAsync
            (
                new LotRegistration()
                {
                    IsOwner = true,
                    LotId = lot.Id,
                    UserId = model.UserIds.FirstOrDefault()
                }
            );
            await _unitOfWork.SaveAsync();
        }

        public async Task DeleteByIdAsync(int modelId)
        {
            await _unitOfWork.LotRepository.DeleteByIdAsync(modelId);
            await _unitOfWork.SaveAsync();
        }

        public IEnumerable<LotModel> GetAll()
        {
            var lots = _unitOfWork.LotRepository.GetAllWithDetails().AsEnumerable();
            return _mapper.Map<IEnumerable<Lot>, IEnumerable<LotModel>>(lots);
        }

        public async Task<LotModel> GetByIdAsync(int id)
        {
            return _mapper.Map<Lot, LotModel>(await _unitOfWork.LotRepository.GetByIdWithDetailsAsync(id));
        }

        public IEnumerable<LotModel> GetLotsByCarModel(string model)
        {
            var lots = _unitOfWork.LotRepository.GetAllWithDetails().Where(x => x.Car.CarDetails.Submodel.Model.ModelName == model).AsEnumerable();
            return _mapper.Map<IEnumerable<Lot>, IEnumerable<LotModel>>(lots);
        }

        public IEnumerable<LotModel> GetLotsByCountry(CountryModel country)
        {
            if (String.IsNullOrEmpty(country.Name))
                throw new PAuctionException("Country name should not be empty");

            var lots = _unitOfWork.LotRepository.GetAllWithDetails().Where(x => x.Country.Name == country.Name).AsEnumerable();
            return _mapper.Map<IEnumerable<Lot>, IEnumerable<LotModel>>(lots);
        }

        public IEnumerable<LotModel> GetLotsByStartTime(DateTime startTime)
        {
            var lots = _unitOfWork.LotRepository.GetAllWithDetails().Where(x => x.StartTime == startTime).AsEnumerable();
            return _mapper.Map<IEnumerable<Lot>, IEnumerable<LotModel>>(lots);
        }

        public async Task UpdateAsync(LotModel model)
        {
            if (model.StartTime == default || model.CurrentPrice == default || model.BetTimeInHours == 0)
                throw new PAuctionException("Lot model does not containt correct data.");
            var lot = _mapper.Map<LotModel, Lot>(model);
            _unitOfWork.LotRepository.Update(lot);
            await _unitOfWork.SaveAsync();
        }
    }
}
