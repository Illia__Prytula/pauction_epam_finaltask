﻿using AutoMapper;
using PAuction.BL.Interfaces;
using PAuction.BL.Models;
using PAuction.BL.Validation;
using PAuction.DAL.Entities;
using PAuction.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PAuction.BL.Services
{
    public class CarService : ICarService
    {
        readonly IUnitOfWork _unitOfWork;
        readonly IMapper _mapper;
        public CarService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task AddAsync(CarModel model)
        {
            // need test: if adding car is it adding a cardetails => submodels => models etc.
            if (String.IsNullOrEmpty(model.VIN) || String.IsNullOrEmpty(model.Transmission) || String.IsNullOrEmpty(model.SubmodelName) ||
                    String.IsNullOrEmpty(model.SubmodelDescription) || String.IsNullOrEmpty(model.Odometr) || String.IsNullOrEmpty(model.ModelName) ||
                    String.IsNullOrEmpty(model.Fuel) || String.IsNullOrEmpty(model.CarName) || String.IsNullOrEmpty(model.Body) || String.IsNullOrEmpty(model.AdditionalInfo) ||
                    model.Year < 1000)
                throw new PAuctionException("Car model is not valid");

            var car = _mapper.Map<CarModel, Car>(model);
            await _unitOfWork.CarRepository.AddAsync(car);
            await _unitOfWork.SaveAsync();
        }

        public async Task DeleteByIdAsync(int modelId)
        {
            await _unitOfWork.CarRepository.DeleteByIdAsync(modelId);
            await _unitOfWork.SaveAsync();
        }

        public IEnumerable<CarModel> GetAll()
        {
            var cars = _unitOfWork.CarRepository.GetAllWithDetails().AsEnumerable();
            return _mapper.Map<IEnumerable<Car>, IEnumerable<CarModel>>(cars);
        }

        public async Task<CarModel> GetByIdAsync(int id)
        {
            return _mapper.Map<Car, CarModel>(await _unitOfWork.CarRepository.GetByIdWithDetailsAsync(id));
        }

        public IEnumerable<CarModel> SearchByFilter(CarFilterSearchModel filter)
        {
            var cars = _unitOfWork.CarRepository.GetAllWithDetails()
                .Where(x => x.CarDetails.Year == filter.Year || x.CarDetails.Odometr == filter.Odometr
                        || x.CarDetails.Submodel.Model.ModelName == filter.ModelName || x.Damaged == filter.Damaged 
                        || x.CarDetails.Submodel.SubmodelName == filter.SubmodelName)
                .AsEnumerable();

            return _mapper.Map<IEnumerable<Car>, IEnumerable<CarModel>>(cars);
        }

        public async Task UpdateAsync(CarModel model)
        {
            if (String.IsNullOrEmpty(model.VIN) || String.IsNullOrEmpty(model.Transmission) || String.IsNullOrEmpty(model.SubmodelName) ||
                    String.IsNullOrEmpty(model.SubmodelDescription) || String.IsNullOrEmpty(model.Odometr) || String.IsNullOrEmpty(model.ModelName) ||
                    String.IsNullOrEmpty(model.Fuel) || String.IsNullOrEmpty(model.CarName) || String.IsNullOrEmpty(model.Body) || String.IsNullOrEmpty(model.AdditionalInfo) ||
                    model.Year < 1000)
                throw new PAuctionException("Car model is not valid");

            var car = _mapper.Map<CarModel, Car>(model);
            _unitOfWork.CarRepository.Update(car);
            await _unitOfWork.SaveAsync();
        }
    }
}
