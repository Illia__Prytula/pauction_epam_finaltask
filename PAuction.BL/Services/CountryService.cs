﻿using AutoMapper;
using PAuction.BL.Interfaces;
using PAuction.BL.Models;
using PAuction.BL.Validation;
using PAuction.DAL.Entities;
using PAuction.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PAuction.BL.Services
{
    public class CountryService : ICrud<CountryModel>
    {
        readonly IUnitOfWork _unitOfWork;
        readonly IMapper _mapper;
        public CountryService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task AddAsync(CountryModel model)
        {
            if (String.IsNullOrEmpty(model.Name))
                throw new PAuctionException("Country name should not be empty");
            var country = _mapper.Map<CountryModel, Country>(model);
            await _unitOfWork.CountryRepository.AddAsync(country);
            await _unitOfWork.SaveAsync();
        }

        public async Task DeleteByIdAsync(int modelId)
        {
            await _unitOfWork.CountryRepository.DeleteByIdAsync(modelId);
            await _unitOfWork.SaveAsync();
        }

        public IEnumerable<CountryModel> GetAll()
        {
            var countries = _unitOfWork.CountryRepository.GetAll().AsEnumerable();
            return _mapper.Map<IEnumerable<Country>, IEnumerable<CountryModel>>(countries);
        }

        public async Task<CountryModel> GetByIdAsync(int id)
        {
            return _mapper.Map<Country, CountryModel>(await _unitOfWork.CountryRepository.GetByIdAsync(id));
        }

        public async Task UpdateAsync(CountryModel model)
        {
            if (String.IsNullOrEmpty(model.Name))
                throw new PAuctionException("Country name should not be empty");
            var country = _mapper.Map<CountryModel, Country>(model);
            _unitOfWork.CountryRepository.Update(country);
            await _unitOfWork.SaveAsync();
        }
    }
}
