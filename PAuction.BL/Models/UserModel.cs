﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PAuction.BL.Models
{
    public class UserModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public DateTime RegistrationDate { get; set; }
        public int Reputation { get; set; }
        public ICollection<int> LotIds { get; set; }
    }
}
