﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PAuction.BL.Models
{
    public class LotModel
    {
        public int Id { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime LastBetTime { get; set; }
        public int BetTimeInHours { get; set; }
        public decimal CurrentPrice { get; set; }
        public decimal RegistrationFee { get; set; }
        public bool IsSold { get; set; }
        public int CountryId { get; set; }
        public ICollection<int> UserIds { get; set; }
    }
}
