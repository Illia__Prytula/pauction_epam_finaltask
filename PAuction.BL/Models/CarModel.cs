﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PAuction.BL.Models
{
    public class CarModel
    {
        public int Id { get; set; }
        public string CarName { get; set; }
        public bool Damaged { get; set; }
        public string VIN { get; set; }
        public string AdditionalInfo { get; set; }
        public int Year { get; set; }
        public string Body { get; set; }
        public string Fuel { get; set; }
        public string Transmission { get; set; }
        public string Odometr { get; set; }
        public string ModelName { get; set; }
        public string SubmodelName { get; set; }
        public string SubmodelDescription { get; set; }
        public int LotId { get; set; }
    }
}
