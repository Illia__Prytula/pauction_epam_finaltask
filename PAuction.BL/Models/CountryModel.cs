﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PAuction.BL.Models
{
    public class CountryModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<int> LotIds { get; set; }
    }
}
