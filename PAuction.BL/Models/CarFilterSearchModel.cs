﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PAuction.BL.Models
{
    public class CarFilterSearchModel
    {
        public bool Damaged { get; set; }
        public int Year { get; set; }
        public string Odometr { get; set; }
        public string ModelName { get; set; }
        public string SubmodelName { get; set; }
    }
}
