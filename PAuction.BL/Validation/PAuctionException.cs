﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PAuction.BL.Validation
{
    public class PAuctionException : Exception
    {
        public PAuctionException() : base() { }

        public PAuctionException(string msg) : base(msg)
        {
        }
        public PAuctionException(string message, Exception inner)
        : base(message, inner)
        {
        }
    }
}
