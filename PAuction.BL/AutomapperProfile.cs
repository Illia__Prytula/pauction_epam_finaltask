﻿using AutoMapper;
using PAuction.BL.Models;
using PAuction.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PAuction.BL
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            CreateMap<Car, CarModel>()
                .ForMember(cm => cm.Year, c => c.MapFrom(car => car.CarDetails.Year))
                .ForMember(cm => cm.Body, c => c.MapFrom(car => car.CarDetails.Body))
                .ForMember(cm => cm.Fuel, c => c.MapFrom(car => car.CarDetails.Fuel))
                .ForMember(cm => cm.Transmission, c => c.MapFrom(car => car.CarDetails.Transmission))
                .ForMember(cm => cm.Odometr, c => c.MapFrom(car => car.CarDetails.Odometr))
                .ForMember(cm => cm.ModelName, c => c.MapFrom(car => car.CarDetails.Submodel.Model.ModelName))
                .ForMember(cm => cm.SubmodelName, c => c.MapFrom(car => car.CarDetails.Submodel.SubmodelName))
                .ForMember(cm => cm.SubmodelDescription, c => c.MapFrom(car => car.CarDetails.Submodel.Description))
                .ReverseMap();

            CreateMap<Country, CountryModel>()
                .ForMember(cm => cm.LotIds, c => c.MapFrom(country => country.Lots.Select(x => x.Id)))
                .ReverseMap();

            CreateMap<Lot, LotModel>()
                 .ForMember(um => um.UserIds, u => u.MapFrom(user => user.Users.Select(x => x.UserId)))
                .ReverseMap();

            CreateMap<User, UserModel>()
                 .ForMember(um => um.LotIds, u => u.MapFrom(user => user.Lots.Select(x => x.LotId)))
                 .ReverseMap();
        }
    }
}
