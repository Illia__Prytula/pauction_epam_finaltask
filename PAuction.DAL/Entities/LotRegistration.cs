﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace PAuction.DAL.Entities
{
    public class LotRegistration : DBEntity
    {
        public bool IsOwner { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public int LotId { get; set; }
        public Lot Lot { get; set; } 
    }
}
