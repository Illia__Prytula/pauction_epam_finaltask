﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PAuction.DAL.Entities
{
    public class Country : DBEntity
    {
        public string Name { get; set; }
        public ICollection<Lot> Lots { get; set; }
    }
}
