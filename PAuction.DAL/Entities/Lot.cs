﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace PAuction.DAL.Entities
{
    public class Lot : DBEntity
    {
        public DateTime StartTime { get; set; }
        public DateTime LastBetTime { get; set; }
        public int BetTimeInHours { get; set; }
        [Column(TypeName = "decimal(18,4)")]
        public decimal CurrentPrice { get; set; }
        [Column(TypeName = "decimal(18,4)")]
        public decimal RegistrationFee { get; set; }
        public bool IsSold { get; set; }
        public int CountryId { get; set; }
        public Country Country { get; set; }
        public Car Car { get; set; }
        public ICollection<LotRegistration> Users { get; set; }
    }
}
