﻿using System;
using System.Collections.Generic;

namespace PAuction.DAL.Entities
{
    public class User : DBEntity
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public DateTime RegistrationDate { get; set; }
        public int Reputation { get; set; }
        public string HashedPassword { get; set; }
        public string Salt { get; set; }
        public ICollection<LotRegistration> Lots { get; set; }
    }
}