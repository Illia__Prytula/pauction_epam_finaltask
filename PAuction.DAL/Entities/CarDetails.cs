﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PAuction.DAL.Entities
{
    public class CarDetails : DBEntity
    {
        public int Year { get; set; }
        public int CarId { get; set; }
        public Car Car { get; set; }
        public int SubmodelId { get; set; }
        public Submodel Submodel { get; set; }
        public string Body { get; set; }
        public string Fuel { get; set; }
        public string Transmission { get; set; }
        public string Odometr { get; set; }
    }
}
