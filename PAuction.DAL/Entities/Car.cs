﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PAuction.DAL.Entities
{
    public class Car : DBEntity
    {
        public string CarName { get; set; }
        public bool Damaged { get; set; }
        public string VIN { get; set; }
        public CarDetails CarDetails { get; set; }
        public string AdditionalInfo { get; set; }
        public int LotId { get; set; }
        public Lot Lot { get; set; }
    }
}
