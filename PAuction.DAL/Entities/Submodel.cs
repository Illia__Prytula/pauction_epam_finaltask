﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PAuction.DAL.Entities
{
    public class Submodel : DBEntity
    {
        public string SubmodelName { get; set; }
        public string Description { get; set; }
        public int ModelId { get; set; }
        public Model Model { get; set; }
        public ICollection<CarDetails> CarDetails { get; set; }
    }
}
