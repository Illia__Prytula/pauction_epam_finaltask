﻿using System.Collections.Generic;

namespace PAuction.DAL.Entities
{
    public class Model : DBEntity
    {
        public string ModelName { get; set; }
        public ICollection<Submodel> Submodels { get; set; }
    }
}