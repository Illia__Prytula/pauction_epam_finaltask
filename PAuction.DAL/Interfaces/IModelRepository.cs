﻿using PAuction.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PAuction.DAL.Interfaces
{
    public interface IModelRepository : IRepository<Model>
    {
        Task<Model> GetByIdWithDetailsAsync(int id);
        IQueryable<Model> GetAllWithDetails();
    }
}
