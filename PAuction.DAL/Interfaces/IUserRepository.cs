﻿using PAuction.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PAuction.DAL.Interfaces
{
    public interface IUserRepository : IRepository<User>
    {
        Task<User> GetByIdWithDetailsAsync(int id);
        IQueryable<User> GetAllWithDetails();
    }
}
