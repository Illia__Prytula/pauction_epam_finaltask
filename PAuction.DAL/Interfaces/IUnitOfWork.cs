﻿using PAuction.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PAuction.DAL.Interfaces
{
    public interface IUnitOfWork
    {
        IRepository<Country> CountryRepository { get; }
        ICarDetailsRepository CarDetailsRepository { get; }
        ICarRepository CarRepository { get; }
        IRepository<LotRegistration> LotRegistrationRepository { get; }
        ILotRepository LotRepository { get; }
        IModelRepository ModelRepository { get; }
        IUserRepository UserRepository { get; }
        Task<int> SaveAsync();
    }
}
