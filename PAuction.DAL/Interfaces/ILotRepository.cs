﻿using PAuction.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PAuction.DAL.Interfaces
{
    public interface ILotRepository : IRepository<Lot>
    {
        Task<Lot> GetByIdWithDetailsAsync(int id);
        IQueryable<Lot> GetAllWithDetails();
    }
}
