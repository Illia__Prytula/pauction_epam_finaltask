﻿using PAuction.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PAuction.DAL.Interfaces
{
    public interface ICarDetailsRepository : IRepository<CarDetails>
    {
        IQueryable<CarDetails> GetAllWithDetails();
    }
}
