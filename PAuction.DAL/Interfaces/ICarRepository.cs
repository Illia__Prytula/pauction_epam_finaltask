﻿using PAuction.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PAuction.DAL.Interfaces
{
    public interface ICarRepository : IRepository<Car>
    {
        Task<Car> GetByIdWithDetailsAsync(int id);
        IQueryable<Car> GetAllWithDetails();
    }
}
