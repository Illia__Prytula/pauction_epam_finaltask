﻿using PAuction.DAL.Entities;
using PAuction.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PAuction.DAL.Repositories
{
    public class LotRegistrationRepository : IRepository<LotRegistration>
    {
        readonly PAuctionDBContext _context;
        public LotRegistrationRepository(PAuctionDBContext context)
        {
            _context = context;
        }
        public Task AddAsync(LotRegistration entity)
        {
            return _context.AddAsync(entity).AsTask();
        }

        public void Delete(LotRegistration entity)
        {
            _context.LotRegistrations.Remove(entity);
        }

        public async Task DeleteByIdAsync(int id)
        {
            var entity = await _context.LotRegistrations.FindAsync(id);

            if (entity != null)
            {
                _context.LotRegistrations.Remove(entity);
            }
        }

        public IQueryable<LotRegistration> GetAll()
        {
            return _context.LotRegistrations;
        }

        public Task<LotRegistration> GetByIdAsync(int id)
        {
            return _context.LotRegistrations.FindAsync(id).AsTask();
        }

        public void Update(LotRegistration entity)
        {
            _context.LotRegistrations.Update(entity);
        }


    }
}
