﻿using Microsoft.EntityFrameworkCore;
using PAuction.DAL.Entities;
using PAuction.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PAuction.DAL.Repositories
{
    public class CarDetailsRepository : ICarDetailsRepository
    { 
        readonly PAuctionDBContext _context;
        public CarDetailsRepository(PAuctionDBContext context)
        {
            _context = context;
        }
        public Task AddAsync(CarDetails entity)
        {
            return _context.AddAsync(entity).AsTask();
        }

        public void Delete(CarDetails entity)
        {
            _context.CarDetails.Remove(entity);
        }

        public async Task DeleteByIdAsync(int id)
        {
            var entity = await _context.CarDetails.FindAsync(id);

            if (entity != null)
            {
                _context.CarDetails.Remove(entity);
            }
        }

        public IQueryable<CarDetails> GetAll()
        {
            return _context.CarDetails;
        }

        public IQueryable<CarDetails> GetAllWithDetails()
        {
            return _context.CarDetails.Include(x => x.Submodel).Include(x => x.Car);
        }

        public Task<CarDetails> GetByIdAsync(int id)
        {
            return _context.CarDetails.FindAsync(id).AsTask();
        }

        public void Update(CarDetails entity)
        {
            _context.CarDetails.Update(entity);
        }
    }
}
