﻿using Microsoft.EntityFrameworkCore;
using PAuction.DAL.Entities;
using PAuction.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PAuction.DAL.Repositories
{
    public class CountryRepository : IRepository<Country>
    {
        readonly PAuctionDBContext _context;
        public CountryRepository(PAuctionDBContext context)
        {
            _context = context;
        }

        public Task AddAsync(Country entity)
        {
            return _context.AddAsync(entity).AsTask();
        }

        public void Delete(Country entity)
        {
            _context.Countries.Remove(entity);
        }

        public async Task DeleteByIdAsync(int id)
        {
            var entity = await _context.Countries.FindAsync(id);

            if (entity != null)
            {
                _context.Countries.Remove(entity);
            }
        }

        public IQueryable<Country> GetAll()
        {
            return _context.Countries.Include(x => x.Lots);
        }

        public Task<Country> GetByIdAsync(int id)
        {
            return _context.Countries.Include(x => x.Lots).FirstOrDefaultAsync(x => x.Id == id);
        }

        public void Update(Country entity)
        {
            _context.Countries.Update(entity);
        }

    }
}
