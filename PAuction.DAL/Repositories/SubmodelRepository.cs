﻿using PAuction.DAL.Entities;
using PAuction.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PAuction.DAL.Repositories
{
    public class SubmodelRepository : IRepository<Submodel>
    {
        readonly PAuctionDBContext _context;
        public SubmodelRepository(PAuctionDBContext context)
        {
            _context = context;
        }
        public Task AddAsync(Submodel entity)
        {
            return _context.AddAsync(entity).AsTask();
        }

        public void Delete(Submodel entity)
        {
            _context.Submodels.Remove(entity);
        }

        public async Task DeleteByIdAsync(int id)
        {
            var entity = await _context.Submodels.FindAsync(id);

            if (entity != null)
            {
                _context.Submodels.Remove(entity);
            }
        }

        public IQueryable<Submodel> GetAll()
        {
            return _context.Submodels;
        }

        public Task<Submodel> GetByIdAsync(int id)
        {
            return _context.Submodels.FindAsync(id).AsTask();
        }

        public void Update(Submodel entity)
        {
            _context.Submodels.Update(entity);
        }
    }
}
