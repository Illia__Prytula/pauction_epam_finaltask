﻿using Microsoft.EntityFrameworkCore;
using PAuction.DAL.Entities;
using PAuction.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PAuction.DAL.Repositories
{
    public class LotRepository : ILotRepository
    {
        readonly PAuctionDBContext _context;
        public LotRepository(PAuctionDBContext context)
        {
            _context = context;
        }
        public Task AddAsync(Lot entity)
        {
            return _context.AddAsync(entity).AsTask();
        }

        public void Delete(Lot entity)
        {
            _context.Lots.Remove(entity);
        }

        public async Task DeleteByIdAsync(int id)
        {
            var entity = await _context.Lots.FindAsync(id);

            if (entity != null)
            {
                _context.Lots.Remove(entity);
            }
        }

        public IQueryable<Lot> GetAll()
        {
            return _context.Lots;
        }

        public IQueryable<Lot> GetAllWithDetails()
        {
            return _context.Lots.Include(x => x.Country).Include(x => x.Users).Include(x => x.Car);
        }

        public Task<Lot> GetByIdAsync(int id)
        {
            return _context.Lots.FindAsync(id).AsTask();
        }

        public Task<Lot> GetByIdWithDetailsAsync(int id)
        {
            return _context.Lots.Include(x => x.Country).Include(x => x.Users).Include(x => x.Car).FirstOrDefaultAsync(x => x.Id == id);
        }

        public void Update(Lot entity)
        {
            _context.Lots.Update(entity);
        }
    }
}
