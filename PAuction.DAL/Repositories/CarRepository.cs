﻿using Microsoft.EntityFrameworkCore;
using PAuction.DAL.Entities;
using PAuction.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PAuction.DAL.Repositories
{
    public class CarRepository : ICarRepository
    {
        readonly PAuctionDBContext _context;
        public CarRepository(PAuctionDBContext context)
        {
            _context = context;
        }
        public Task AddAsync(Car entity)
        {
            return _context.AddAsync(entity).AsTask();
        }

        public void Delete(Car entity)
        {
            _context.Cars.Remove(entity);
        }

        public async Task DeleteByIdAsync(int id)
        {
            var entity = await _context.Cars.FindAsync(id);

            if (entity != null)
            {
                _context.Cars.Remove(entity);
            }
        }

        public IQueryable<Car> GetAll()
        {
            return _context.Cars;
        }

        public IQueryable<Car> GetAllWithDetails()
        {
            return _context.Cars.Include(x => x.CarDetails.Submodel.Model);
        }

        public Task<Car> GetByIdAsync(int id)
        {
            return _context.Cars.FindAsync(id).AsTask();
        }

        public Task<Car> GetByIdWithDetailsAsync(int id)
        {
            return _context.Cars.Include(x => x.CarDetails.Submodel.Model).FirstOrDefaultAsync(x => x.Id == id);
        }

        public void Update(Car entity)
        {
            _context.Cars.Update(entity);
        }
    }
}
