﻿using Microsoft.EntityFrameworkCore;
using PAuction.DAL.Entities;
using PAuction.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PAuction.DAL.Repositories
{
    public class UserRepository : IUserRepository
    {
        readonly PAuctionDBContext _context;
        public UserRepository(PAuctionDBContext context)
        {
            _context = context;
        }
        public Task AddAsync(User entity)
        {
            return _context.AddAsync(entity).AsTask();
        }

        public void Delete(User entity)
        {
            _context.Users.Remove(entity);
        }

        public async Task DeleteByIdAsync(int id)
        {
            var entity = await _context.Users.FindAsync(id);

            if (entity != null)
            {
                _context.Users.Remove(entity);
            }
        }

        public IQueryable<User> GetAll()
        {
            return _context.Users;
        }

        public IQueryable<User> GetAllWithDetails()
        {
            return _context.Users.Include(x => x.Lots);
        }

        public Task<User> GetByIdAsync(int id)
        {
            return _context.Users.FindAsync(id).AsTask();
        }

        public Task<User> GetByIdWithDetailsAsync(int id)
        {
            return _context.Users.Include(x => x.Lots).FirstOrDefaultAsync(x => x.Id == id);
        }

        public void Update(User entity)
        {
            _context.Users.Update(entity);
        }

    }
}
