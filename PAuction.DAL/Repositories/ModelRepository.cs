﻿using Microsoft.EntityFrameworkCore;
using PAuction.DAL.Entities;
using PAuction.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PAuction.DAL.Repositories
{
    public class ModelRepository : IModelRepository
    {
        readonly PAuctionDBContext _context;
        public ModelRepository(PAuctionDBContext context)
        {
            _context = context;
        }
        public Task AddAsync(Model entity)
        {
            return _context.AddAsync(entity).AsTask();
        }

        public void Delete(Model entity)
        {
            _context.Models.Remove(entity);
        }

        public async Task DeleteByIdAsync(int id)
        {
            var entity = await _context.Models.FindAsync(id);

            if (entity != null)
            {
                _context.Models.Remove(entity);
            }
        }

        public IQueryable<Model> GetAll()
        {
            return _context.Models;
        }

        public IQueryable<Model> GetAllWithDetails()
        {
            return _context.Models.Include(x => x.Submodels);
        }

        public Task<Model> GetByIdAsync(int id)
        {
            return _context.Models.FindAsync(id).AsTask();
        }

        public Task<Model> GetByIdWithDetailsAsync(int id)
        {
            return _context.Models.Include(x => x.Submodels).FirstOrDefaultAsync(x => x.Id == id);
        }

        public void Update(Model entity)
        {
            _context.Models.Update(entity);
        }
    }
}
