﻿using PAuction.DAL.Entities;
using PAuction.DAL.Interfaces;
using PAuction.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PAuction.DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        readonly PAuctionDBContext db;

        public UnitOfWork(PAuctionDBContext context)
        {
            db = context;
        }

        private CarDetailsRepository carDetailsRepository;
        public ICarDetailsRepository CarDetailsRepository
        {
            get
            {
                if (carDetailsRepository is null)
                    carDetailsRepository = new CarDetailsRepository(db);
                return carDetailsRepository;
            }
        }

        private CarRepository carRepository;
        public ICarRepository CarRepository
        {
            get
            {
                if (carRepository is null)
                    carRepository = new CarRepository(db);
                return carRepository;
            }
        }

        private CountryRepository countryRepository;
        public IRepository<Country> CountryRepository
        {
            get
            {
                if (countryRepository is null)
                    countryRepository = new CountryRepository(db);
                return countryRepository;
            }
        }

        private LotRegistrationRepository lotRegistrationRepository;
        public IRepository<LotRegistration> LotRegistrationRepository
        {
            get
            {
                if (lotRegistrationRepository is null)
                    lotRegistrationRepository = new LotRegistrationRepository(db);
                return lotRegistrationRepository;
            }
        }

        private LotRepository lotRepository;
        public ILotRepository LotRepository
        {
            get
            {
                if (lotRepository is null)
                    lotRepository = new LotRepository(db);
                return lotRepository;
            }
        }

        private ModelRepository modelRepository;
        public IModelRepository ModelRepository
        {
            get
            {
                if (modelRepository is null)
                    modelRepository = new ModelRepository(db);
                return modelRepository;
            }
        }

        private SubmodelRepository submodelRepository;
        public IRepository<Submodel> SubmodelRepository
        {
            get
            {
                if (submodelRepository is null)
                    submodelRepository = new SubmodelRepository(db);
                return submodelRepository;
            }
        }

        private UserRepository userRepository;
        public IUserRepository UserRepository
        {
            get
            {
                if (userRepository is null)
                    userRepository = new UserRepository(db);
                return userRepository;
            }
        }

        public Task<int> SaveAsync()
        {
            return db.SaveChangesAsync();
        }
    }
}
