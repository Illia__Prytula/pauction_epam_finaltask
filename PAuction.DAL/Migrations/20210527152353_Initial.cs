﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PAuction.DAL.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Countries",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Countries", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Models",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ModelName = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Models", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Surname = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Phone = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    RegistrationDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Reputation = table.Column<int>(type: "int", nullable: false),
                    HashedPassword = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Salt = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Lots",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    StartTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    LastBetTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    BetTimeInHours = table.Column<int>(type: "int", nullable: false),
                    CurrentPrice = table.Column<decimal>(type: "decimal(18,4)", nullable: false),
                    RegistrationFee = table.Column<decimal>(type: "decimal(18,4)", nullable: false),
                    IsSold = table.Column<bool>(type: "bit", nullable: false),
                    CountryId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Lots", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Lots_Countries_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Countries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Submodels",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SubmodelName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ModelId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Submodels", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Submodels_Models_ModelId",
                        column: x => x.ModelId,
                        principalTable: "Models",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Cars",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CarName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Damaged = table.Column<bool>(type: "bit", nullable: false),
                    VIN = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    AdditionalInfo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LotId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cars", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Cars_Lots_LotId",
                        column: x => x.LotId,
                        principalTable: "Lots",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "LotRegistrations",
                columns: table => new
                {
                    UserId = table.Column<int>(type: "int", nullable: false),
                    LotId = table.Column<int>(type: "int", nullable: false),
                    IsOwner = table.Column<bool>(type: "bit", nullable: false),
                    Id = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LotRegistrations", x => new { x.UserId, x.LotId });
                    table.ForeignKey(
                        name: "FK_LotRegistrations_Lots_LotId",
                        column: x => x.LotId,
                        principalTable: "Lots",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_LotRegistrations_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CarDetails",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Year = table.Column<int>(type: "int", nullable: false),
                    CarId = table.Column<int>(type: "int", nullable: false),
                    SubmodelId = table.Column<int>(type: "int", nullable: false),
                    Body = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Fuel = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Transmission = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Odometr = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CarDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CarDetails_Cars_CarId",
                        column: x => x.CarId,
                        principalTable: "Cars",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CarDetails_Submodels_SubmodelId",
                        column: x => x.SubmodelId,
                        principalTable: "Submodels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Countries",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Ukraine" },
                    { 2, "Poland" },
                    { 3, "Turkey" },
                    { 4, "Romania" },
                    { 5, "Germany" },
                    { 6, "France" },
                    { 7, "Italy" }
                });

            migrationBuilder.InsertData(
                table: "Models",
                columns: new[] { "Id", "ModelName" },
                values: new object[,]
                {
                    { 1, "Mercedes-Benz" },
                    { 2, "BMW" },
                    { 3, "Skoda" },
                    { 4, "Honda" },
                    { 5, "Mazda" },
                    { 6, "Toyota" },
                    { 7, "Volkswagen" },
                    { 8, "Renault" }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Email", "HashedPassword", "Name", "Phone", "RegistrationDate", "Reputation", "Salt", "Surname" },
                values: new object[] { 1, "IlliaPrytula@gmail.com", "WVdSdGFXND1KK0Z2TjJKVE54ZklsaThVMStMNFB3PT0=", "Illia", "38066987777", new DateTime(2021, 5, 27, 18, 23, 52, 636, DateTimeKind.Local).AddTicks(5723), 100, "J+FvN2JTNxfIli8U1+L4Pw==", "Prytula" });

            migrationBuilder.InsertData(
                table: "Lots",
                columns: new[] { "Id", "BetTimeInHours", "CountryId", "CurrentPrice", "IsSold", "LastBetTime", "RegistrationFee", "StartTime" },
                values: new object[,]
                {
                    { 1, 24, 1, 2000m, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 100m, new DateTime(2021, 8, 15, 16, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 5, 36, 1, 2000m, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 200m, new DateTime(2021, 7, 10, 18, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 6, 10, 1, 6000m, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 300m, new DateTime(2021, 12, 1, 17, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 2, 36, 2, 3000m, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 150m, new DateTime(2021, 8, 25, 13, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 7, 12, 2, 2000m, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 200m, new DateTime(2021, 9, 13, 16, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 3, 24, 3, 2000m, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 200m, new DateTime(2021, 8, 15, 12, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 4, 24, 4, 5000m, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 100m, new DateTime(2021, 8, 16, 15, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 8, 10, 5, 3000m, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 100m, new DateTime(2021, 8, 14, 15, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 9, 5, 6, 4000m, false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 150m, new DateTime(2021, 8, 5, 10, 0, 0, 0, DateTimeKind.Unspecified) }
                });

            migrationBuilder.InsertData(
                table: "Submodels",
                columns: new[] { "Id", "Description", "ModelId", "SubmodelName" },
                values: new object[,]
                {
                    { 10, "Toyota Land Cruiser Pardo", 6, "Land Cruiser" },
                    { 9, "Mazda cx-5 descr", 5, "CX-5" },
                    { 8, "Honda jazz descr", 4, "Jazz" },
                    { 7, "Skoda rapid descr", 3, "Rapid" },
                    { 6, "Skoda octavia descr", 3, "Octavia" },
                    { 2, "s class", 1, "S" },
                    { 4, "2 series", 2, "Z" },
                    { 3, "1 series", 2, "X" },
                    { 11, "VW Jetta", 7, "Jetta" },
                    { 1, "class 400 4matic coupe amg", 1, "GLE" },
                    { 5, "1 series", 2, "M" },
                    { 12, "Reanult sandero", 8, "Sandero" }
                });

            migrationBuilder.InsertData(
                table: "Cars",
                columns: new[] { "Id", "AdditionalInfo", "CarName", "Damaged", "LotId", "VIN" },
                values: new object[,]
                {
                    { 1, "Mers not damaged", "Mers-Benz 2000", false, 1, "12345671" },
                    { 8, "Mazda not damaged", "Mazda CX-5", false, 8, "12345678" },
                    { 4, "BMW not damaged", "BMW X3", false, 4, "12345674" },
                    { 3, "Honda, not damaged", "Honda Rapid 2004", false, 3, "12345673" },
                    { 9, "BMW not damaged", "BMW X5", false, 9, "12345679" },
                    { 2, "Skoda-Octavia damaged right front door", "Skoda Octavia 2005", true, 2, "12345672" },
                    { 7, "Renault not damaged", "Renault Sandero", false, 7, "12345677" },
                    { 6, "Toyota not damaged", "Toyota Land Cruiser", false, 6, "12345676" },
                    { 5, "Crankshaft is in bad condition, needs wheels aligning", "Volkswagen Jetta", true, 5, "12345675" }
                });

            migrationBuilder.InsertData(
                table: "LotRegistrations",
                columns: new[] { "LotId", "UserId", "Id", "IsOwner" },
                values: new object[,]
                {
                    { 2, 1, 2, true },
                    { 7, 1, 7, true },
                    { 5, 1, 5, true },
                    { 3, 1, 3, true },
                    { 4, 1, 4, true },
                    { 1, 1, 1, true },
                    { 8, 1, 8, true },
                    { 6, 1, 6, true },
                    { 9, 1, 9, true }
                });

            migrationBuilder.InsertData(
                table: "CarDetails",
                columns: new[] { "Id", "Body", "CarId", "Fuel", "Odometr", "SubmodelId", "Transmission", "Year" },
                values: new object[,]
                {
                    { 1, "Sedan", 1, "Gasoline", "1234512km", 1, "Manual", 2000 },
                    { 5, "Hatchback", 5, "Diesel", "54512km", 11, "Manual", 2005 },
                    { 6, "Sedan", 6, "Gasoline", "534512km", 10, "Manual", 2005 },
                    { 2, "Sedan", 2, "Gasoline", "11212km", 6, "Automatic", 2005 },
                    { 7, "Hatchback", 7, "Gasoline", "24512km", 12, "Manual", 2007 },
                    { 3, "Sedan", 3, "Diesel", "112312km", 7, "Manual", 2004 },
                    { 4, "Sedan", 4, "Gasoline", "35512km", 3, "Automatic", 2010 },
                    { 8, "Sedan", 8, "Gasoline", "13212km", 9, "Automatic", 2015 },
                    { 9, "Jeep", 9, "Gasoline", "12512km", 3, "Automatic", 2015 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_CarDetails_CarId",
                table: "CarDetails",
                column: "CarId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_CarDetails_SubmodelId",
                table: "CarDetails",
                column: "SubmodelId");

            migrationBuilder.CreateIndex(
                name: "IX_Cars_LotId",
                table: "Cars",
                column: "LotId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_LotRegistrations_LotId",
                table: "LotRegistrations",
                column: "LotId");

            migrationBuilder.CreateIndex(
                name: "IX_Lots_CountryId",
                table: "Lots",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_Submodels_ModelId",
                table: "Submodels",
                column: "ModelId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CarDetails");

            migrationBuilder.DropTable(
                name: "LotRegistrations");

            migrationBuilder.DropTable(
                name: "Cars");

            migrationBuilder.DropTable(
                name: "Submodels");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Lots");

            migrationBuilder.DropTable(
                name: "Models");

            migrationBuilder.DropTable(
                name: "Countries");
        }
    }
}
