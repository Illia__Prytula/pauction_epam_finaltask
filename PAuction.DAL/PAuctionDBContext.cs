﻿using Microsoft.EntityFrameworkCore;
using PAuction.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace PAuction.DAL
{
    public class PAuctionDBContext : DbContext
    {
        public DbSet<Car> Cars { get; set; }
        public DbSet<CarDetails> CarDetails { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Lot> Lots { get; set; }
        public DbSet<LotRegistration> LotRegistrations { get; set; }
        public DbSet<Model> Models { get; set; }
        public DbSet<Submodel> Submodels { get; set; }
        public DbSet<User> Users { get; set; }

        public PAuctionDBContext(DbContextOptions<PAuctionDBContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<LotRegistration>()
                .HasKey(ur => new { ur.UserId, ur.LotId });

            modelBuilder.Entity<LotRegistration>()
                .HasOne(ur => ur.User)
                .WithMany(u => u.Lots)
                .HasForeignKey(u => u.UserId);

            modelBuilder.Entity<LotRegistration>()
                .HasOne(ur => ur.Lot)
                .WithMany(r => r.Users)
                .HasForeignKey(r => r.LotId);


            modelBuilder.Entity<Model>().HasData
            (
                new Model() { Id = 1, ModelName = "Mercedes-Benz" },
                new Model() { Id = 2, ModelName = "BMW" },
                new Model() { Id = 3, ModelName = "Skoda" },
                new Model() { Id = 4, ModelName = "Honda" },
                new Model() { Id = 5, ModelName = "Mazda" },
                new Model() { Id = 6, ModelName = "Toyota" },
                new Model() { Id = 7, ModelName = "Volkswagen" },
                new Model() { Id = 8, ModelName = "Renault" }
            );

            modelBuilder.Entity<Submodel>().HasData
            (
                new Submodel() { Id = 1, SubmodelName = "GLE", ModelId = 1, Description = "class 400 4matic coupe amg" },
                new Submodel() { Id = 2, SubmodelName = "S", ModelId = 1, Description = "s class" },
                new Submodel() { Id = 3, SubmodelName = "X", ModelId = 2, Description = "1 series" },
                new Submodel() { Id = 4, SubmodelName = "Z", ModelId = 2, Description = "2 series" },
                new Submodel() { Id = 5, SubmodelName = "M", ModelId = 2, Description = "1 series" },
                new Submodel() { Id = 6, SubmodelName = "Octavia", ModelId = 3, Description = "Skoda octavia descr" },
                new Submodel() { Id = 7, SubmodelName = "Rapid", ModelId = 3, Description = "Skoda rapid descr" },
                new Submodel() { Id = 8, SubmodelName = "Jazz", ModelId = 4, Description = "Honda jazz descr" },
                new Submodel() { Id = 9, SubmodelName = "CX-5", ModelId = 5, Description = "Mazda cx-5 descr" },
                new Submodel() { Id = 10, SubmodelName = "Land Cruiser", ModelId = 6, Description = "Toyota Land Cruiser Pardo" },
                new Submodel() { Id = 11, SubmodelName = "Jetta", ModelId = 7, Description = "VW Jetta" },
                new Submodel() { Id = 12, SubmodelName = "Sandero", ModelId = 8, Description = "Reanult sandero" }
            );

            modelBuilder.Entity<Country>().HasData
            (
                new Country() { Id = 1, Name = "Ukraine"},
                new Country() { Id = 2, Name = "Poland" },
                new Country() { Id = 3, Name = "Turkey" },
                new Country() { Id = 4, Name = "Romania" },
                new Country() { Id = 5, Name = "Germany" },
                new Country() { Id = 6, Name = "France" },
                new Country() { Id = 7, Name = "Italy" }
            );

            modelBuilder.Entity<Lot>().HasData
            (
                new Lot() { Id = 1, CountryId = 1, IsSold = false, RegistrationFee = 100, StartTime = new DateTime(2021, 08, 15, 16, 0, 0), BetTimeInHours = 24, CurrentPrice = 2000 },
                new Lot() { Id = 2, CountryId = 2, IsSold = false, RegistrationFee = 150, StartTime = new DateTime(2021, 08, 25, 13, 0, 0), BetTimeInHours = 36, CurrentPrice = 3000 },
                new Lot() { Id = 3, CountryId = 3, IsSold = false, RegistrationFee = 200, StartTime = new DateTime(2021, 08, 15, 12, 0, 0), BetTimeInHours = 24, CurrentPrice = 2000 },
                new Lot() { Id = 4, CountryId = 4, IsSold = false, RegistrationFee = 100, StartTime = new DateTime(2021, 08, 16, 15, 0, 0), BetTimeInHours = 24, CurrentPrice = 5000 },
                new Lot() { Id = 5, CountryId = 1, IsSold = false, RegistrationFee = 200, StartTime = new DateTime(2021, 07, 10, 18, 0, 0), BetTimeInHours = 36, CurrentPrice = 2000 },
                new Lot() { Id = 6, CountryId = 1, IsSold = false, RegistrationFee = 300, StartTime = new DateTime(2021, 12, 1, 17, 0, 0), BetTimeInHours = 10, CurrentPrice = 6000 },
                new Lot() { Id = 7, CountryId = 2, IsSold = false, RegistrationFee = 200, StartTime = new DateTime(2021, 09, 13, 16, 0, 0), BetTimeInHours = 12, CurrentPrice = 2000 },
                new Lot() { Id = 8, CountryId = 5, IsSold = false, RegistrationFee = 100, StartTime = new DateTime(2021, 08, 14, 15, 0, 0), BetTimeInHours = 10, CurrentPrice = 3000 },
                new Lot() { Id = 9, CountryId = 6, IsSold = false, RegistrationFee = 150, StartTime = new DateTime(2021, 08, 5, 10, 0, 0), BetTimeInHours = 5, CurrentPrice = 4000 }
            );

            modelBuilder.Entity<User>().HasData
            (
                new User() { Id = 1, Name = "Illia", Surname = "Prytula", Email = "IlliaPrytula@gmail.com", Phone = "38066987777", Reputation = 100, RegistrationDate = DateTime.Now, HashedPassword = "WVdSdGFXND1KK0Z2TjJKVE54ZklsaThVMStMNFB3PT0=", Salt = "J+FvN2JTNxfIli8U1+L4Pw==" }
            );

            modelBuilder.Entity<LotRegistration>().HasData
            (
                new LotRegistration() { Id = 1, IsOwner = true, LotId = 1, UserId = 1},
                new LotRegistration() { Id = 2, IsOwner = true, LotId = 2, UserId = 1 },
                new LotRegistration() { Id = 3, IsOwner = true, LotId = 3, UserId = 1 },
                new LotRegistration() { Id = 4, IsOwner = true, LotId = 4, UserId = 1 },
                new LotRegistration() { Id = 5, IsOwner = true, LotId = 5, UserId = 1 },
                new LotRegistration() { Id = 6, IsOwner = true, LotId = 6, UserId = 1 },
                new LotRegistration() { Id = 7, IsOwner = true, LotId = 7, UserId = 1 },
                new LotRegistration() { Id = 8, IsOwner = true, LotId = 8, UserId = 1 },
                new LotRegistration() { Id = 9, IsOwner = true, LotId = 9, UserId = 1 }
            );

            modelBuilder.Entity<Car>().HasData
            (
                new Car() { Id = 1, CarName = "Mers-Benz 2000", Damaged = false, LotId = 1, VIN = "12345671", AdditionalInfo = "Mers not damaged"},
                new Car() { Id = 2, CarName = "Skoda Octavia 2005", Damaged = true, LotId = 2, VIN = "12345672", AdditionalInfo = "Skoda-Octavia damaged right front door"},
                new Car() { Id = 3, CarName = "Honda Rapid 2004", Damaged = false, LotId = 3, VIN = "12345673", AdditionalInfo = "Honda, not damaged"},
                new Car() { Id = 4, CarName = "BMW X3", Damaged = false, LotId = 4, VIN = "12345674", AdditionalInfo = "BMW not damaged"},
                new Car() { Id = 5, CarName = "Volkswagen Jetta", Damaged = true, LotId = 5, VIN = "12345675", AdditionalInfo = "Crankshaft is in bad condition, needs wheels aligning"},
                new Car() { Id = 6, CarName = "Toyota Land Cruiser", Damaged = false, LotId = 6, VIN = "12345676", AdditionalInfo = "Toyota not damaged"},
                new Car() { Id = 7, CarName = "Renault Sandero", Damaged = false, LotId = 7, VIN = "12345677", AdditionalInfo = "Renault not damaged"},
                new Car() { Id = 8, CarName = "Mazda CX-5", Damaged = false, LotId = 8, VIN = "12345678", AdditionalInfo = "Mazda not damaged"},
                new Car() { Id = 9, CarName = "BMW X5", Damaged = false, LotId = 9, VIN = "12345679", AdditionalInfo = "BMW not damaged" }
            );


            modelBuilder.Entity<CarDetails>().HasData
            (
                new CarDetails() { Id = 1, CarId = 1, Body = "Sedan", Fuel = "Gasoline", Odometr = "1234512km", Transmission = "Manual", Year = 2000, SubmodelId = 1},
                new CarDetails() { Id = 2, CarId = 2, Body = "Sedan", Fuel = "Gasoline", Odometr = "11212km", Transmission = "Automatic", Year = 2005, SubmodelId = 6},
                new CarDetails() { Id = 3, CarId = 3, Body = "Sedan", Fuel = "Diesel", Odometr = "112312km", Transmission = "Manual", Year = 2004, SubmodelId = 7},
                new CarDetails() { Id = 4, CarId = 4, Body = "Sedan", Fuel = "Gasoline", Odometr = "35512km", Transmission = "Automatic", Year = 2010, SubmodelId = 3},
                new CarDetails() { Id = 5, CarId = 5, Body = "Hatchback", Fuel = "Diesel", Odometr = "54512km", Transmission = "Manual", Year = 2005, SubmodelId = 11},
                new CarDetails() { Id = 6, CarId = 6, Body = "Sedan", Fuel = "Gasoline", Odometr = "534512km", Transmission = "Manual", Year = 2005, SubmodelId = 10},
                new CarDetails() { Id = 7, CarId = 7, Body = "Hatchback", Fuel = "Gasoline", Odometr = "24512km", Transmission = "Manual", Year = 2007, SubmodelId = 12},
                new CarDetails() { Id = 8, CarId = 8, Body = "Sedan", Fuel = "Gasoline", Odometr = "13212km", Transmission = "Automatic", Year = 2015, SubmodelId = 9},
                new CarDetails() { Id = 9, CarId = 9, Body = "Jeep", Fuel = "Gasoline", Odometr = "12512km", Transmission = "Automatic", Year = 2015, SubmodelId = 3}
            );
        }
    }
}
