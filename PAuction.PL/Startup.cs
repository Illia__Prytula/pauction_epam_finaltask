using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using PAuction.BL;
using PAuction.BL.Interfaces;
using PAuction.BL.Models;
using PAuction.BL.Services;
using PAuction.DAL;
using PAuction.DAL.Entities;
using PAuction.DAL.Interfaces;
using PAuction.DAL.Repositories;

namespace PAuction.PL
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddDbContextPool<PAuctionDBContext>(c => c.UseSqlServer(Configuration.GetConnectionString("PAuctionDB")));
            
            services.AddScoped<IUnitOfWork, UnitOfWork>();

            services.AddScoped<ICarDetailsRepository, CarDetailsRepository>();
            services.AddScoped<ICarRepository, CarRepository>();
            services.AddScoped<IRepository<Country>, CountryRepository>();
            services.AddScoped<ILotRepository, LotRepository>();
            services.AddScoped<IModelRepository, ModelRepository>();
            services.AddScoped<IRepository<Submodel>, SubmodelRepository>();
            services.AddScoped<IUserRepository, UserRepository>();

            services.AddScoped<ICarService, CarService>();
            services.AddScoped<ICrud<CountryModel>, CountryService>();
            services.AddScoped<ILotService, LotService>();
            services.AddScoped<IUserService, UserService>();

            var mpcfg = new MapperConfiguration(c => c.AddProfile(new AutomapperProfile()));
            services.AddSingleton(mpcfg.CreateMapper());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
