﻿using Microsoft.AspNetCore.Mvc;
using PAuction.BL.Interfaces;
using PAuction.BL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PAuction.PL.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        readonly IUserService _service;
        public UsersController(IUserService userService)
        {
            _service = userService;
        }

        //GET: /api/users/
        [HttpGet]
        public ActionResult<IEnumerable<UserModel>> GetAll()
        {
            var result = _service.GetAll();
            return Ok(result);
        }

        //GET: /api/users/1
        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<UserModel>>> GetById(int id)
        {
            var result = await _service.GetByIdAsync(id);
            return Ok(result);
        }

        //POST: /api/users/
        [HttpPost]
        public async Task<ActionResult> Add([FromBody] UserModel model)
        {
            try
            {
                await _service.AddAsync(model);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return CreatedAtAction(nameof(Add), new { model.Id }, model);
        }

        //PUT: /api/users/
        [HttpPut]
        public async Task<ActionResult> Update(UserModel model)
        {
            try
            {
                await _service.UpdateAsync(model);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Ok();
        }

        //DELETE: /api/users/1
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            await _service.DeleteByIdAsync(id);
            return Ok();
        }

    }
}
