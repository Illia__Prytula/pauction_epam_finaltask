﻿using Microsoft.AspNetCore.Mvc;
using PAuction.BL.Interfaces;
using PAuction.BL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PAuction.PL.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class LotsController : ControllerBase
    {
        readonly ILotService _service;
        public LotsController(ILotService lotService)
        {
            _service = lotService;
        }

        //GET: /api/lots/
        [HttpGet]
        public ActionResult<IEnumerable<LotModel>> GetAll()
        {
            var result = _service.GetAll();
            return Ok(result);
        }

        //GET: /api/lots/1
        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<LotModel>>> GetById(int id)
        {
            var result = await _service.GetByIdAsync(id);
            return Ok(result);
        }

        //POST: /api/lots/
        [HttpPost]
        public async Task<ActionResult> Add([FromBody] LotModel model)
        {
            try
            {
                await _service.AddAsync(model);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return CreatedAtAction(nameof(Add), new { model.Id }, model);
        }

        //PUT: /api/lots/
        [HttpPut]
        public async Task<ActionResult> Update(LotModel model)
        {
            try
            {
                await _service.UpdateAsync(model);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Ok();
        }

        //DELETE: /api/lots/1
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            await _service.DeleteByIdAsync(id);
            return Ok();
        }

    }
}
