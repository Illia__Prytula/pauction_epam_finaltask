﻿using Microsoft.AspNetCore.Mvc;
using PAuction.BL.Interfaces;
using PAuction.BL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PAuction.PL.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class CountriesController : ControllerBase
    {
        readonly ICrud<CountryModel> _service;
        public CountriesController(ICrud<CountryModel> countryService)
        {
            _service = countryService;
        }

        //GET: /api/countries/
        [HttpGet]
        public ActionResult<IEnumerable<CountryModel>> GetAll()
        {
            var result = _service.GetAll();
            return Ok(result);
        }

        //GET: /api/countries/1
        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<CountryModel>>> GetById(int id)
        {
            var result = await _service.GetByIdAsync(id);
            return Ok(result);
        }

        //POST: /api/countries/
        [HttpPost]
        public async Task<ActionResult> Add([FromBody] CountryModel model)
        {
            try
            {
                await _service.AddAsync(model);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return CreatedAtAction(nameof(Add), new { model.Id }, model);
        }

        //PUT: /api/countries/
        [HttpPut]
        public async Task<ActionResult> Update(CountryModel model)
        {
            try
            {
                await _service.UpdateAsync(model);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Ok();
        }

        //DELETE: /api/countries/1
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            await _service.DeleteByIdAsync(id);
            return Ok();
        }
    }
}
