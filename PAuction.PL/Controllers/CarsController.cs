﻿using Microsoft.AspNetCore.Mvc;
using PAuction.BL.Interfaces;
using PAuction.BL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PAuction.PL.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class CarsController : ControllerBase
    {
        readonly ICarService _service;
        public CarsController(ICarService carService)
        {
            _service = carService;
        }

        //GET: /api/cars/
        [HttpGet]
        public ActionResult<IEnumerable<CarModel>> GetAll()
        {
            var result = _service.GetAll();
            return Ok(result);
        }

        //GET: /api/cars/1
        [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<CarModel>>> GetById(int id)
        {
            var result = await _service.GetByIdAsync(id);
            return Ok(result);
        }

        //POST: /api/cars/
        [HttpPost]
        public async Task<ActionResult> Add([FromBody] CarModel model)
        {
            try
            {
                await _service.AddAsync(model);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return CreatedAtAction(nameof(Add), new { model.Id }, model);
        }

        //PUT: /api/cars/
        [HttpPut]
        public async Task<ActionResult> Update(CarModel model)
        {
            try
            {
                await _service.UpdateAsync(model);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Ok();
        }

        //DELETE: /api/cars/1
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            await _service.DeleteByIdAsync(id);
            return Ok();
        }
    }
}
